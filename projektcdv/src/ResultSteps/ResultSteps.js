
import React, { Component } from 'react';
import Helmet from 'react-helmet';
import '../App.css';
import { Link } from 'react-router-dom';
import './ResultSteps.css';
import arrow_left from '../img/arrow-left.png';
import { connect } from 'react-redux';
import { clear_state } from "../action";
const mdb = require("../Api/ApiConfig")

class ResultSteps extends Component {
   
    state = {
        movie: []
      };
    
    componentDidMount() {
    //STEP 1

    var age = this.props.location.answer1;

var selectedAge;

switch (age)
{
    case (age < 6):
    selectedAge = '0';
    break;

    case (age >= 6 && age < 12):
    selectedAge = '6';
    break;

    case (age >= 12 && age < 16):
    selectedAge = '12';
    break;

    case(age >= 16 && age < 18):
    selectedAge = '16';
     break;

    case(age >= 18):
    selectedAge = '18';
    break;
}

    var ageGroups = ['0','6','12','16','18'];
  


    //STEP 2

    //for test purposes only
    var year = 2010;

    var yearQueryParams = {};
    yearQueryParams['primary_release_date.gte'] = [''];
    yearQueryParams['primary_release_date.lte'] = [''];
    yearQueryParams['primary_release_year'] = [0];

    switch(this.props.location.answer2)
    {
        case 'before my birth':
           yearQueryParams['primary_release_date.gte'] = (year-5);
           yearQueryParams['primary_release_date.lte'] =  year;
            break;

        case 'my-year':
                yearQueryParams['primary_release_year'] = year;
            break;

        case 'a little younger':          
                yearQueryParams['primary_release_date.gte'] = year;
                yearQueryParams['primary_release_date.lte'] = year + 5;
            break;

        case 'contemporary':
                yearQueryParams['primary_release_date.gte'] = year + 25;
            break;
    }


    //STEP 3
    var genreDictionary = {};
    genreDictionary["smile"] = ["35", "16", "10751", "10402", "37"];
    genreDictionary["scary"] = ["27", "53"];
    genreDictionary["surprise"] = ["28", "80", "14", "9648", "878", "12"];
    genreDictionary["touch"] = ["18", "36", "10752", "10749"];
     
    var selectedGenre;

    switch (this.props.location.answer3)
    {
       case 'smile':
       selectedGenre =  genreDictionary["smile"][Math.floor((Math.random() * 4) + 0)]
     
       break;

       case 'scary':
       selectedGenre = genreDictionary["scary"][Math.floor((Math.random() * 1) + 0)];

       break;

       case 'surprise': 
       selectedGenre = genreDictionary["surprise"][Math.floor((Math.random() * 5) + 0)];

       break;

       case 'touch':
       selectedGenre = genreDictionary["touch"][Math.floor((Math.random() * 3) + 0)];

       break;
    }  

    mdb.discoverMovie({ ['primary_release_year']: yearQueryParams['primary_release_year'], 
    ["primary_release_date.lte"]: yearQueryParams['primary_release_date.gte'],
     ["primary_release_date.lte"]: yearQueryParams['primary_release_date.lte'], 
    genre: selectedGenre, certification_country: 'DE', certification: selectedAge, page : Math.floor(Math.random() * 1000) + 1}, (err, movie) => {
        
     document.getElementById('img1').src =  'https://image.tmdb.org/t/p/w600_and_h900_bestv2'+ movie.results[ Math.floor(Math.random() * 20) + 0].poster_path;
     document.getElementById('img2').src =  'https://image.tmdb.org/t/p/w600_and_h900_bestv2'+ movie.results[ Math.floor(Math.random() * 20) + 0].poster_path;
     this.props.clear_state();
       
    });
}


    render() {
        return (
            <div className="resultSteps">
                <Helmet bodyAttributes={{
                    style: 'background-color : #133d4e'
                }} />
                <p className="question-second">Wybraliśmy dla Ciebie dwa fantastyczne filmy!</p>
                <p className="question-second">Wybór tego idealnego należy do Ciebie!</p>
                    

                <div className="arrows-container">
                    <Link to='/'>
                        <div className="arrow-prev">
                            <img src={arrow_left} alt="" className="arrow arrow-left" />
                        </div>
                    </Link>
                </div>
                <div id="container"> 
                    <div id="resultArea">
                        <div className="resultElemLeft">
                            <img id="img1" />
                        </div>     
                        <div className="resultElemRight">
                            <img id="img2" />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = {
        clear_state: clear_state
}

export default connect(null, mapDispatchToProps)(ResultSteps);
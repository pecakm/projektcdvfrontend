export const SET_AGE = 'SET_AGE';
export const SET_MOOD ='SET_MOOD';
export const SET_MOVIE_AGE = "SET_MOVIE_AGE";
export const CLEAR_STATE = "CLEAR_STATE";


export const set_age = (age) => {
    return {
        type: SET_AGE,
        payload: age
    }
}

export const set_mood = (mood) => {
    return {
        type: SET_MOOD,
        payload: mood
    }
}

export const set_movie_age = (movie_age) =>{
    return {
        type: SET_MOVIE_AGE,
        payload: movie_age
    }
}

export const clear_state = () => {
    return {
        type: CLEAR_STATE
    }
}